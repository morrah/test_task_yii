<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\UserSearch;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\helpers\Url;
use yii\helpers\Html;

use common\components\LanguageSelector;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'language' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
      $searchParams = [];
      $userModel = new User();
      $searchModel = new UserSearch();
      if($field=Yii::$app->request->post('field','username')){
        if($term=Yii::$app->request->post('term')){
          $searchParams['UserSearch'][$field] = $term;
        }
      }

      $dataProvider = $searchModel->search($searchParams);

        return $this->render('index',
      [
        'title' => Yii::t('common', 'Main page title'),
        'leftMenu' => true,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'term' => $term,
        'url' => Yii::$app->request->url,
        'title' => Yii::t('user', 'Search'),
        'radioList' => ['id'=>'id','username'=>'username','email'=>'email','status'=>'status'],//$userModel::attributeLabels(),
        'field' => $field,
      ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about',[
          'uploadPath' => Yii::$app->params['uploadPath'],
          'leftMenu' => true,
        ]);
    }

    /**
     * Displays static page.
     *
     * @param string $url
     * @return mixed
     */
    public function actionPages($url=null)
    {
      $url = (string)$url;

      $staticPagesPath = Yii::$app->params['staticPagesPath'];
      if(empty($url)){
        $content = Yii::$app->request->url.'<br/>';//Yii::t('page','Static pages list');
        if($dh = @opendir($staticPagesPath)){
          while (false !== ($entry = readdir($dh))) {
            if($entry != "." && $entry != "..") {
              $content = $content . Html::a($entry,Yii::$app->request->url . DIRECTORY_SEPARATOR . strrpos($entry,'.')!==false?substr($entry,0,strrpos($entry,'.')):strlen($enrty)). '<br/>';
            }
          }
        }
        closedir($dh);
        return $this->render('static_page',[
          'title' => Yii::t('common','Static pages list'),
          'uploadPath' => Yii::$app->params['uploadPath'],
          'content' => $content,
          'leftMenu' => true,
        ]);
      }

      $pagePath = $staticPagesPath . DIRECTORY_SEPARATOR . $url . '.php';
      if(!file_exists($pagePath)){
        //throw new HttpException(404 ,Yii::t('common', 'Page not found'));
        throw new \yii\web\NotFoundHttpException;
      }
        return $this->render('static_page',[
          'title' => Yii::t('common','Static page'),
          'uploadPath' => Yii::$app->params['uploadPath'],
          'pagePath' => $pagePath,
          'leftMenu' => true,
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionLanguage()
    {
      $language = Yii::$app->request->post('language');
      LanguageSelector::setCookieLanguage($language);
      return $this->redirect(Yii::$app->getRequest()->referrer);
    }
}
