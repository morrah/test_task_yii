<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
$this->params['leftMenu'] = isset($leftMenu)?$leftMenu:null;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'email',
            'status',
            ['label' => Yii::t('common', 'Thumbnail image'), 'content' => function($data){
               //$res =
               if(!empty($data['thumbnail']['thumbs'])){
                 $res = @unserialize($data['thumbnail']['thumbs']);
                 if(isset($res['small'])){
                   $res = $res['small'];
                 }
               }
               else{
                 $res = !empty($data['thumbnail']['url'])?$data['thumbnail']['url']:null;
               }
               if($res){
                 $res = Html::img($res);
               }
              return $res;
            }],
            'created_at:datetime',
            //'updated_at',

            /*['class' => 'yii\grid\ActionColumn',
              'template' => '{view}',
            ],*/
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
