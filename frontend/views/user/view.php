<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['leftMenu'] = isset($leftMenu)?$leftMenu:null;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email',
            'status',
            ['label' => Yii::t('common', 'Thumbnail image'), 'value' => (!empty($model->thumbnail)?(Html::img($model->thumbnail->getThumbUrl('medium'))):''), 'format' => 'html'],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
