<?php
/* @var $this yii\web\View */
use yii\jui\AutoComplete;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = $title;
$this->params['breadcrumbs'][] = ['url' => '/user','label' => Yii::t('user','User')];
$this->params['breadcrumbs'][] = $this->title;
$this->params['leftMenu'] = isset($leftMenu)?$leftMenu:null;
?>
<h1><?= $title ?></h1>

<?php

echo Html::beginForm($url,'post');

echo AutoComplete::widget([
  'name' => 'term',
  'value' => $term,
  'options' => [],
  'clientOptions' => [
    //'source' => Url::toRoute('autocomplete'),
    'source' =>  new JsExpression('function(request,response){
      field=jQuery(document.getElementById("field_radio")).find("input:checked").val();
      if(!field){
        field = "username";
      }
      jQuery.get(
      "'.Url::toRoute('/user/autocomplete').'",
      {term:request.term,field:field},
      function(data){response(data)}
    );
  }'),
    'dataType' => 'json',
    'autoFill' => true,
    'minLength' => 1,
    'select' => new JsExpression('function(event, ui) {
        this.value = ui.item.label;
    }'),
  ],

]);
echo Html::radioList('field',$field, $radioList, ['id' => 'field_radio']);
echo Html::submitButton(Yii::t('common','Search'));
echo Html::endForm();
?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'email',
            'status',
            ['label' => Yii::t('common', 'Thumbnail image'), 'content' => function($data){
               //$res =
               if(!empty($data['thumbnail']['thumbs'])){
                 $res = @unserialize($data['thumbnail']['thumbs']);
                 if(isset($res['small'])){
                   $res = $res['small'];
                 }
               }
               else{
                 $res = !empty($data['thumbnail']['url'])?$data['thumbnail']['url']:null;
               }
               if($res){
                 $res = Html::img($res);
               }
              return $res;
            }],
            'created_at:datetime',
            // 'updated_at',

            /*['class' => 'yii\grid\ActionColumn',
              'template' => '{view}',
            ],*/
        ],
        'filterPosition' => '',
    ]); ?>
<?php Pjax::end(); ?>
