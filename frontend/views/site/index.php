<?php

/* @var $this yii\web\View */

use yii\widgets\Menu;
use common\components\Leftmenu;

$this->title = $title;
$this->params['leftMenu'] = isset($leftMenu)?$leftMenu:null;
?>
<div class="site-index">

    <div class="body-content">

                <h2><?= Yii::t('common', 'Search employeee') ?></h2>

                <?= Yii::t('common', 'Set criteria. Type searchphrase.') ?>

                <?= $this->render('@frontend/views/user/search', [
                  'title' => $title,
                  'leftMenu' => $leftMenu,
                  'searchModel' => $searchModel,
                  'dataProvider' => $dataProvider,
                  'term' => $term,
                  'url' => $url,
                  'title' => $title,
                  'radioList' => $radioList,
                  'field' => $field,
                ]) ?>
    </div>
</div>
