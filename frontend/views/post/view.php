<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('page', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['leftMenu'] = isset($leftMenu)?$leftMenu:null;
?>
<div class="page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'id_lang',
            'keywords',
            ['label' => Yii::t('common', 'Thumbnail image'), 'value' => (!empty($model->thumbnail)?(Html::img($model->thumbnail->getThumbUrl('medium'))):''), 'format' => 'html'],
            'content:html',
            ['label' => Yii::t('common', 'Created by'), 'value' => (!empty($model->user)?(Html::a($model->user->username, Url::toRoute(['/user/view', 'id' => $model->id_user]))):''), 'format' => 'html'],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
