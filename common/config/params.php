<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'uploadPath' => dirname(dirname(__DIR__)) . '/upload',
    'staticPagesPath' => dirname(dirname(__DIR__)) . '/upload/static_pages',
    'supportedLanguages' => ['en-US', 'ru-RU'],
    'mainSiteUrl' => 'http://www.nsu.ru',
    'headerLogo' => '/upload/common/logo.png',
    'user_groups' => [
      '1' => 'admin',
      '2' => 'moderator',
      '3' => 'author',
      '4' => 'user'
    ],
    'default_user_group' => '4',
];
