<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'components' => [
      'cache' => [
          'class' => 'yii\caching\FileCache',
      ],
      'i18n' => [
        'translations' => [
            '*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                //'basePath' => '@common/messages',
                'sourceLanguage' => 'en-US',
                //'language' => 'ru-RU',
                'fileMap' => [
                    'app' => 'app.php',
                    'app/error' => 'error.php',
                ],
            ],
        ],
      ],
    ],
    'bootstrap' => [
    [
        'class' => 'common\components\LanguageSelector',
        'supportedLanguages' => ['en_US', 'ru_RU'],
    ],
    [
      'class' => 'common\components\SimpleACL',
      'acl' => [
       'app-backend' => [
       'checked_by' => 'group_id',
       'id' => [
            1,
          ],
        'group_id' => [1],
        ]
      ]
    ],
  ],
  'modules' => [
    'filemanager' => [
        'class' => 'pendalf89\filemanager\Module',
        // Upload routes
        'routes' => [
            // Base absolute path to web directory
            'baseUrl' => '',
            // Base web directory url
            'basePath' => '@frontend/web',
            // Path for uploaded files in web directory
            'uploadPath' => 'upload',
        ],
        // Thumbnails info
        'thumbs' => [
            'small' => [
                'name' => 'Small',
                'size' => [100, 100],
            ],
            'medium' => [
                'name' => 'Medium',
                'size' => [300, 200],
            ],
            'large' => [
                'name' => 'Large',
                'size' => [500, 400],
            ],
        ],
    ],
  ],
];
