<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use pendalf89\filemanager\behaviors\MediafileBehavior;
use pendalf89\filemanager\models\Mediafile;
use pendalf89\filemanager\models\Owners;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $id_lang
 * @property string $keywords
 * @property string $content
 * @property integer $id_user
 * @property integer $created_at
 * @property integer $updated_at
 * @property int | Mediafile $thumbnail
 * @property Owners $owners
 * @property string $thumbnail_url
 */
class Page extends ActiveRecord
{
    private $_thumbnail;
    private $owners;
    //public $thumbnail_url;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::className(),
              'attributes' => [
                  ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                  ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
              ],
            ],

            'mediafile' => [
              'class' => MediafileBehavior::className(),
              'name' => 'Page',
              'attributes' => [
                  'thumbnail',
              ],
            ],

            [
               'class' => BlameableBehavior::className(),
               'createdByAttribute' => 'id_user',
               'updatedByAttribute' => 'id_user',
             ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_lang', 'id_user', 'created_at', 'updated_at'], 'integer'],
            [['content'], 'string'],
            [['title', 'keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('page', 'ID'),
            'title' => Yii::t('page', 'Title'),
            'id_lang' => Yii::t('page', 'Id Lang'),
            'keywords' => Yii::t('page', 'Keywords'),
            'content' => Yii::t('page', 'Content'),
            'id_user' => Yii::t('page', 'Id User'),
            'created_at' => Yii::t('page', 'Created At'),
            'updated_at' => Yii::t('page', 'Updated At'),
            //'thumbnail_url' => Yii::t('page', 'Thumbnail image'),
        ];
    }

    public function getOwners(){
      return $this->hasOne(Owners::className(),['owner_id' => 'id']);
    }

    public function setOwners($owners){
      if($owners){
        if($owners instanceof Owners){
          $this->owners = $owners;
        }
        else {
          $this->owners = Owners::findOne($owners);
        }
      }
      return $this;
    }

    public function getThumbnail(){
      return $this->hasOne(Mediafile::className(),['id' => 'mediafile_id'])
        ->via('owners');
    }

    public function setThumbnail($thumbnail){
      $this->thumbnail = $thumbnail;
      return $this;
    }

    public function getUser(){
      return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /*public function getThumbnailUrl(){
      if($this->thumbnail_url)
        return $this->thumbnail_url;
      return $this->getThumbnail()->getThumbUrl('original');
    }

    public function setThumbnailUrl($thumbnail_url){
      $this->thumbnail_url = $thumbnail_url;
      return $this;
    }*/
}
