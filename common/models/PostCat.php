<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%post_cat}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $id_lang
 * @property integer $id_user
 * @property integer $created_at
 * @property integer $updated_at
 */
class PostCat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post_cat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_lang', 'id_user', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('post_cat', 'ID'),
            'title' => Yii::t('post_cat', 'Title'),
            'id_lang' => Yii::t('post_cat', 'Id Lang'),
            'id_user' => Yii::t('post_cat', 'Id User'),
            'created_at' => Yii::t('post_cat', 'Created At'),
            'updated_at' => Yii::t('post_cat', 'Updated At'),
        ];
    }
}
