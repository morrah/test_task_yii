<?php

use yii\db\Migration;
use common\models\User;

class m160520_172904_user_acl_group extends Migration
{
    public function up()
    {
      $this->addColumn('{{%user}}', 'group_id', $this->integer());
      if(!$user = User::findOne(1)){
        echo "User with ID 1 not found. Creating admin user with password admin0...\n";
        $user = new User();
        $user->username ='admin';
        $user->email = 'admin@example.com';
        $user->setPassword('admin0');
        $user->generateAuthKey();
      }
      $user->group_id = 1;
      if($user->save()){
        echo "User with ID ".$user->id." (".$user->username.") assigned admin access level.\n";
      }

    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'group_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
