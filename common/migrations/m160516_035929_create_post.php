<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%post}}`.
 */
class m160516_035929_create_post extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%post}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'id_lang' => $this->integer(11),
            'id_post_cat' => $this->integer(11),
            'content' => $this->text(),
            'img' => $this->text(),
            'id_user' => $this->integer(11),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%post}}');
    }
}
