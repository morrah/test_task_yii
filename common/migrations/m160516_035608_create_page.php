<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%page}}`.
 */
class m160516_035608_create_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'id_lang' => $this->integer(11),
            'keywords' => $this->string(255),
            'content' => $this->text(),
            'id_user' => $this->integer(11),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%page}}');
    }
}
