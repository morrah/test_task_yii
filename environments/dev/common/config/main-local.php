<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=yii2advanced',
            'username' => 'root',
            'password' => '123123',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'ldap' => [
          'class' => 'Adldap\Adldap',
          'account_suffix'        => '@nodomain',
          'domain_controllers'    => ['localhost'],
          'port'                  => 389,
          'base_dn'               => 'dc=nodomain',
          'admin_username'        => 'admin',
          'admin_password'        => '123123',
          'follow_referrals'      => true,
          'use_ssl'               => false,
          'use_tls'               => false,
          'use_sso'               => false,
        ]
    ],
];
